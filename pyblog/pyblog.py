# !/usr/bin/env python3
import requests
import base64
import argparse
import sys

cred = '''
             _____            ____    _
            |  __ \\          |  _ \\  | |
            | |__) |  _   _  | |_) | | |   ___     __ _
            |  ___/  | | | | |  _ <  | |  / _ \\   / _` |
            | |      | |_| | | |_) | | | | (_) | | (_| |
            |_|       \\__, | |____/  |_|  \\___/   \\__, |
                       __/ |                       __/ |
                      |___/                       |___/        1.0
----------------------------------------------------------------------
By : Billy & Dan
----------------------------------------------------------------------
'''

# Showing Credentials
print(cred)

'''----------------------------------------------------------------------'''

# Auth  (Note: need to use enviormental vars)
auth = 'Basic ' + str(base64.b64encode(b'capstone:capstone'), 'utf-8')
headers = {'Authorization': auth}

# Wordpress URL's (Note: need to use enviormental vars)
url_link_prod = "http://ec2-18-224-195-135.us-east-2.compute.amazonaws.com:8088/wp-json/wp/v2/posts/"  # noqa: E501
url_link_stage = "http://ec2-3-13-184-130.us-east-2.compute.amazonaws.com:8088/wp-json/wp/v2/posts/"   # noqa: E501

# (Note: need to use enviormental vars)
# url = url_link_prod
url = url_link_stage

'''----------------------------------------------------------------------'''

# Interactive User CLI too for WordPress
def cli_wordpress_tool():  # noqa: E302
    # Give the user some context about the PyBlog app.
    print("\nWelcome to the PyBlog. What would you like to do?")

    # Set an initial value for choice other than the value for 'quit'.
    choice = ''

    # Start a loop that runs until the user enters the value for 'quit'.
    while choice != 'q':
        # Give all the choices in a series of print statements.
        print("----------------------------------------------------------------------")  # noqa: E501
        print("\n[1] Enter 1 to Create a new blog post.")
        print("[2] Enter 2 to Retrieve the lastest blog post.")
        print("[3] Enter 3 to Update the lastest blog post.")
        print("[4] Enter 4 to Upload a file as a blog post.")
        print("[5] Enter 5 to Delete lastest blog post.")
        print("[q] Enter q to quit.")
        # Ask for the user's choice.
        choice = input("\nWhat would you like to do? ").lower()
        # Respond to the user's choice.
        if choice == '1':
            title = input("\nPlease enter a Title for this blog post. ")
            content = input("\nPlease enter the Content for this blog post.\n")
            create_wordpress_post(title, content, True)
            post_id = str(get_latest_postid())
        elif choice == '2':
            post_id = str(get_latest_postid())
            get_wordpress_post(post_id)
        elif choice == '3':
            post_id = str(get_latest_postid())
            content = input("\nPlease enter the updated Content for this blog post.\n")  # noqa: E501
            update_wordpress_post(content, post_id)
        elif choice == '4':
            uploadfile = input("\nPlease enter the file name to upload for this blog post.\n")  # noqa: E501
            upload_wordpress_post(uploadfile, True)
            post_id = str(get_latest_postid())
        elif choice == '5':
            post_id = str(get_latest_postid())
            delete_wordpress_post(post_id)
            post_id = str(get_latest_postid())
        elif choice == 'q':
            print("\nThank you, come again!\n")
        else:
            print("\nI don't understand that choice, please try again.")

# Retrieve the latest post ID
def get_latest_postid():  # noqa: E302

    r = requests.get(url, headers=headers)
    allpost_data = r.json()
    if allpost_data == []:
        print('\nThere are no blog posts!\n\n')
    else:
        post_id_data = allpost_data[0]['id']
        # print('The latest post Id # is: ' + str(post_id_data)  + '\n')
        return str(post_id_data)

# Create a post
def create_wordpress_post(title, content, publish):  # noqa: E302

    payload = {
        'title': title,
        'type': 'post',
        'content': content,
        'status': 'publish' if publish else 'draft',
    }
    r = requests.post(url, data=payload, headers=headers)
    status = r.status_code
    # print (r.json)
    if status == 201:
        print('\n\nYour NEW blog post request was Successful')
    else:
        print('\n\nYour NEW blog post request was Unsuccessful - Somthing went wrong!')  # noqa: E501
    print('')
    print('')

# Retrieve the latest post
def get_wordpress_post(post_id):  # noqa: E302

    r = requests.get(url + post_id, headers=headers)
    get_data = r.json()
    status = r.status_code
    if status == 404:
        print('\n\nThere are no blog posts to GET!\n\n')
    else:
        get_title = get_data['title']['rendered']
        get_content = get_data['content']['rendered']
        print('\nblog post #' + post_id + ' was retrieved!\n\nThe Title is:\n' + get_title + '\n')  # noqa: E501
        print('The content is: \n' + get_content)

# Update the latest post
def update_wordpress_post(content, post_id):  # noqa: E302

    updated_data = {'content': content}
    r = requests.post(url + post_id, data=updated_data, headers=headers)
    updated_data = r.json()
    status = r.status_code
    if status == 404:
        print('\n\nThere are no blog posts to Update!\n\n')
    else:
        update_content = updated_data['content']['rendered']
        print('\nBlog post #' + post_id + ' was updated!\n')
        print('The blog post content is now: \n' + update_content)

# Upload a file as the latest post
def upload_wordpress_post(uploadfile, publish):  # noqa: E302

    try:
     with open(uploadfile, "r") as uploaded :  # noqa
        uploadfile = open(uploadfile, "r")
        title = uploadfile.readline()
        content = uploadfile.read()
        uploadfile.close()
        payload = {
         'title': title,
         'type': 'post',
         'content': content,
         'status': 'publish' if publish else 'draft',
        }
        r = requests.post(url, data=payload, headers=headers)
        status = r.status_code
        # print (r.json)
        if status == 201:
            print('\n\nYour NEW uploaded blog post request was Successful')
        else:
            print('\n\nYour NEW uploaded blog post request was Unsuccessful - Somthing went wrong!')  # noqa: E501
        print('')
        print('')
    except FileNotFoundError:
        print('\nFile not found!\n')
        # Continue if file not found

# Delete the latest post
def delete_wordpress_post(post_id):  # noqa: E302

    r = requests.delete(url + post_id, headers=headers)
    # delete_data = r.json()
    status = r.status_code
    if status == 404:
        print('\n\nThere are no blog posts to Delete!\n\n')
    else:
        print('\nBlog post #' + post_id + ' moved to the Trash. (Well, it should be...)\n')  # noqa: E501


'''----------------------------------------------------------------------'''

# Get latest post ID
post_id = str(get_latest_postid())

# Arg parser setup
parser = argparse.ArgumentParser(description='PyBlog is a Cli tool for WordPress - You can use these optional aruments to: upload (requires a filename.txt), read, create, update, or delete the latest posts')  # noqa: E501
subparsers = parser.add_subparsers(description='Use the word "upload" then the filename.txt to upload a new blog post to WordPress')  # noqa: E501
parser_upload = subparsers.add_parser('upload')
parser_read = subparsers.add_parser('read')
parser_create = subparsers.add_parser('create')
parser_update = subparsers.add_parser('update')
parser_delete = subparsers.add_parser('delete')
parser_upload.add_argument('filename', type=argparse.FileType('r'))
args = parser.parse_args()

try:
    if sys.argv[1] == 'upload':
        upload = args.filename.name
        print('The file name uploaded is {}'.format(args.filename.name))
        upload_wordpress_post(upload, True)
    elif sys.argv[1] == 'read':
        get_wordpress_post(post_id)
    elif sys.argv[1] == 'create':
        title = input("\nPlease enter a Title for this blog post. ")
        content = input("\nPlease enter the Content for this blog post.\n")
        create_wordpress_post(title, content, True)
    elif sys.argv[1] == 'update':
        content = input("\nPlease enter the updated Content for this blog post.\n")  # noqa: E501
        update_wordpress_post(content, post_id)
    elif sys.argv[1] == 'delete':
        delete_wordpress_post(post_id)
    else:
        cli_wordpress_tool()
except IndexError:
    cli_wordpress_tool()

'''----------------------------------------------------------------------'''
