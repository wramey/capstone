# !/usr/bin/env python3
import json
import pytest
import unittest
import unittest.mock
import requests
import pyblog


class MockResponse:

    def __init__(self, text, status_code):
        self.text = text
        self.status_code = status_code

    def json(self):
        return json.loads(self.text)

    def __iter__(self):
        return self

    def __next__(self):
        return self


def mock_create_wordpress_post(*args, **kwargs):
    text = """
    {"status":"success",
     "data":{
       'title': title,
        'type': 'post',
        'content': content,
        'status': 'publish'
    }}
    """
    response = MockResponse(text, 201)
    return response

def mock_requests_get_error(*args, **kwargs):
    response = MockResponse("", 500)
    return response


class TestPost(unittest.TestCase):

    @unittest.mock.patch('requests.get', mock_create_wordpress_post)
    def test_create_wordpress_post(self):
        title, content, status = pyblog.create_wordpress_post()
        self.assertEqual("The Title", title)
        self.assertEqual("the content", content)
        self.assertEqual("publish", status)